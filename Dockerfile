FROM        php:8.0.11-fpm
LABEL       maintainer="Wahib LAROUI <wahib@laroui.com>"

RUN         curl -sL https://deb.nodesource.com/setup_18.x | bash - \
            && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
            && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN         apt-get -qq update --fix-missing \
            && apt-get -qq install --no-install-recommends -y \
                build-essential \
                curl \
                cron \
                fontconfig \
                git \
                gnupg \
                libfontconfig \
                libfreetype6-dev \
                libicu-dev \
                libjpeg62-turbo-dev \
                libonig-dev \
                libpng-dev \
                libwebp-dev \
                libxml2-dev \
                libxpm-dev \
                libxrender-dev \
                libzip-dev \
                nginx \
                nodejs \
                software-properties-common \
                supervisor \
                unzip \
                vim \
                wget \
                xfonts-75dpi \
                xfonts-base \
                xvfb \
                yarn \
                zip \
                apache2-utils \
            && rm -rf /var/lib/apt/lists/*

#
# extensions
#
RUN         docker-php-ext-configure gd --with-freetype --with-jpeg \
            && docker-php-ext-install -j "$(nproc)" gd

RUN         docker-php-ext-install \
                iconv \
                intl \
                mbstring \
                mysqli \
                opcache \
                pdo \
                pdo_mysql \
                xml \
                zip \
                exif

RUN         pecl install \
                apcu \
            && docker-php-ext-enable \
                apcu \
                opcache

#
# timezone
#
RUN ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime && echo Europe/Paris > /etc/timezone

#
# composer
#
RUN curl -sS https://getcomposer.org/installer | tee composer-setup.php \
    && php composer-setup.php && rm composer-setup.php* \
    && chmod +x composer.phar && mv composer.phar /usr/bin/composer

#
# composer & node & yarn configs
#
RUN mkdir -p /var/composer \
    && mkdir -p /var/www/.yarn \
    && mkdir -p /var/www/.cache \
    && touch /var/www/.yarnrc \
    && chown -R www-data:www-data /var/www \
    && chown -R www-data:www-data /var/composer

#
# php, php-fpm and nginx config
#
COPY        ./php.conf.d/* /usr/local/etc/php/conf.d/
COPY        ./php-fpm.d/* /usr/local/etc/php-fpm.d/
COPY        ./nginx.conf.d/* /etc/nginx/conf.d/
RUN         rm /etc/nginx/sites-enabled/default

#
# supervisor
#
COPY ./supervisor.d/* /etc/supervisor/conf.d/

CMD ["/usr/bin/supervisord"]
